import os
import uuid

from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_serialize import FlaskSerializeMixin
from gevent.pywsgi import WSGIServer


app = Flask(__name__)
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db/donations.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['UPLOAD_FOLDER'] = '/opt/backend/media'
db = SQLAlchemy(app)

FlaskSerializeMixin.db = db


class Announcement(db.Model, FlaskSerializeMixin):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), nullable=False)
    amount = db.Column(db.Integer, default=0, nullable=False)
    max_sum = db.Column(db.Integer, default=0, nullable=True)
    photo = db.Column(db.String(256), nullable=True)
    target = db.Column(db.String(256),  nullable=False)
    description = db.Column(db.String(512), nullable=False)
    end_date = db.Column(db.Integer, nullable=True)
    vk_user_id = db.Column(db.Integer, nullable=False)

    create_fields = ['title', 'amount', 'max_sum', 'target', 'photo', 'description', 'end_date', 'vk_user_id']
    update_fields = ['amount']


@app.route('/announcement/<int:item_id>', methods=['GET', 'PUT', 'DELETE', 'POST'])
@app.route('/announcement', methods=['GET', 'POST'])
def route_setting_all(item_id=None):
    return Announcement.get_delete_put_post(item_id)


@app.route('/upload_photo', methods=['POST'])
def upload_photo():
    file = request.files['photo']
    name = f'{uuid.uuid4()}.jpg'
    path = os.path.join(app.config['UPLOAD_FOLDER'], name)
    file.save(path)
    return {
        'path': f'/media/{name}'
    }


def main():
    http_server = WSGIServer(('0.0.0.0', 5000), app)
    http_server.serve_forever()


if __name__ == '__main__':
    main()
